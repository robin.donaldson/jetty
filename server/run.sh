#!/bin/sh

sleep 2

# Use cp to copy container m2 folder to container .m2 folder, only copying newer or missing files.
# container .m2 folder is mirrored to host .m2 folder to speed up build times
cp -ru m2/* .m2 &

# Copy jar file into containers target directory and synchronise with hosts target directory
cp *.jar target &

# Start application
java -jar *.jar