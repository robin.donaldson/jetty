package jetty.openapi.models;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.openapitools.jackson.nullable.JsonNullable;
import java.io.Serializable;
import java.time.OffsetDateTime;
import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import jakarta.annotation.Generated;

/**
 * EmployeeAddressDTO
 */

@JsonTypeName("EmployeeAddress")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-07-12T16:08:09.013419200+01:00[Europe/London]")
public class EmployeeAddressDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  @JsonProperty("streetAddress")
  private String streetAddress;

  @JsonProperty("city")
  private String city;

  public EmployeeAddressDTO streetAddress(String streetAddress) {
    this.streetAddress = streetAddress;
    return this;
  }

  /**
   * Get streetAddress
   * @return streetAddress
  */
  
  @Schema(name = "streetAddress", required = false)
  public String getStreetAddress() {
    return streetAddress;
  }

  public void setStreetAddress(String streetAddress) {
    this.streetAddress = streetAddress;
  }

  public EmployeeAddressDTO city(String city) {
    this.city = city;
    return this;
  }

  /**
   * Get city
   * @return city
  */
  
  @Schema(name = "city", required = false)
  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EmployeeAddressDTO employeeAddress = (EmployeeAddressDTO) o;
    return Objects.equals(this.streetAddress, employeeAddress.streetAddress) &&
        Objects.equals(this.city, employeeAddress.city);
  }

  @Override
  public int hashCode() {
    return Objects.hash(streetAddress, city);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class EmployeeAddressDTO {\n");
    sb.append("    streetAddress: ").append(toIndentedString(streetAddress)).append("\n");
    sb.append("    city: ").append(toIndentedString(city)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

