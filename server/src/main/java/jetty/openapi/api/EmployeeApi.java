/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech) (6.0.1).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
package jetty.openapi.api;

import jetty.openapi.models.EmployeeAddressDTO;
import jetty.openapi.models.EmployeeDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import jakarta.annotation.Generated;

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-07-12T16:08:09.013419200+01:00[Europe/London]")
@Validated
@Tag(name = "employee", description = "the employee API")
public interface EmployeeApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    /**
     * GET /employee/address/{employeeId} : Retrieves an employee&#39;s address by employee&#39;s ID
     *
     * @param employeeId Numeric ID of the employee to retrieve employee address (required)
     * @return OK (status code 200)
     *         or Unauthorised (status code 401)
     *         or Employee not found (status code 404)
     */
    @Operation(
        operationId = "employeeAddressEmployeeIdGet",
        summary = "Retrieves an employee's address by employee's ID",
        responses = {
            @ApiResponse(responseCode = "200", description = "OK", content = {
                @Content(mediaType = "application/json", schema = @Schema(implementation = EmployeeAddressDTO.class))
            }),
            @ApiResponse(responseCode = "401", description = "Unauthorised"),
            @ApiResponse(responseCode = "404", description = "Employee not found")
        }
    )
    @RequestMapping(
        method = RequestMethod.GET,
        value = "/employee/address/{employeeId}",
        produces = { "application/json" }
    )
    default ResponseEntity<EmployeeAddressDTO> employeeAddressEmployeeIdGet(
        @Parameter(name = "employeeId", description = "Numeric ID of the employee to retrieve employee address", required = true) @PathVariable("employeeId") Integer employeeId
    ) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"streetAddress\" : \"streetAddress\", \"city\" : \"city\" }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }


    /**
     * GET /employee/{employeeId} : Retrieves an employee by ID
     *
     * @param employeeId Numeric ID of the employee to retrieve (required)
     * @return OK (status code 200)
     *         or Unauthorised (status code 401)
     *         or Employee not found (status code 404)
     */
    @Operation(
        operationId = "employeeEmployeeIdGet",
        summary = "Retrieves an employee by ID",
        responses = {
            @ApiResponse(responseCode = "200", description = "OK", content = {
                @Content(mediaType = "application/json", schema = @Schema(implementation = EmployeeDTO.class))
            }),
            @ApiResponse(responseCode = "401", description = "Unauthorised"),
            @ApiResponse(responseCode = "404", description = "Employee not found")
        }
    )
    @RequestMapping(
        method = RequestMethod.GET,
        value = "/employee/{employeeId}",
        produces = { "application/json" }
    )
    default ResponseEntity<EmployeeDTO> employeeEmployeeIdGet(
        @Parameter(name = "employeeId", description = "Numeric ID of the employee to retrieve", required = true) @PathVariable("employeeId") Integer employeeId
    ) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"firstName\" : \"firstName\", \"lastName\" : \"lastName\", \"age\" : 0 }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

}
