package jetty.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class ExceptionDTO {

    private String exceptionName = "";
    private String code = "";
    private String message = "";

}
