package jetty.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value={AuthenticationException.class})
    public ResponseEntity<ExceptionDTO> handleAuthenticationException(Exception exception) {
        ExceptionDTO exceptionDTO = new ExceptionDTO("Unauthorised", UNAUTHORIZED.toString(), "User is not authorised");
        return new ResponseEntity<>(exceptionDTO, UNAUTHORIZED);
    }

    @ExceptionHandler(value= {NotFoundException.class})
    public ResponseEntity<ExceptionDTO> handleNotFoundException(Exception exception) {
        ExceptionDTO exceptionDTO = new ExceptionDTO("Not Found", NOT_FOUND.toString(), exception.getMessage());
        return new ResponseEntity<>(exceptionDTO, NOT_FOUND);
    }

}
