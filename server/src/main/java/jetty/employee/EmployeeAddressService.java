package jetty.employee;

import jetty.exception.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeAddressService {

    public EmployeeAddress findEmployeeAddressById(Integer id) {

        List<EmployeeAddress> employeeAddresses = new ArrayList<>();

        employeeAddresses.add(new EmployeeAddress(1, "2 King William Mews", "NOTTINGHAM"));
        employeeAddresses.add(new EmployeeAddress(2, "86 Kings Road", "BROMLEY"));
        employeeAddresses.add(new EmployeeAddress(3, "740 Queens Road", "TWICKENHAM"));
        employeeAddresses.add(new EmployeeAddress(4,"241 Park Road", "WORCESTER"));
        employeeAddresses.add(new EmployeeAddress(5,"85 New Street", "CAMBRIDGE"));
        employeeAddresses.add(new EmployeeAddress(6,"98 North Road", "BLACKBURN"));
        employeeAddresses.add(new EmployeeAddress(7,"63 Mill Lane", "TRURO"));
        employeeAddresses.add(new EmployeeAddress(8,"57 The Drive", "STEVENAGE"));
        employeeAddresses.add(new EmployeeAddress(9,"76 Richmond Road", "LERWICK"));
        employeeAddresses.add(new EmployeeAddress(10,"94 Station Road", "HEREFORD"));

        return employeeAddresses.stream()
                .filter(employeeAddress -> employeeAddress.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new NotFoundException("Address for employee ID: " + id + " not found!"));

    }

}
