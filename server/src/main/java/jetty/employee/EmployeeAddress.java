package jetty.employee;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class EmployeeAddress {

    Integer id;

    String streetAddress = "";

    String city = "";

}
