package jetty.employee;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Employee {

    Integer id;

    String firstName = "";

    String lastName = "";

    Integer age = 0;

}
