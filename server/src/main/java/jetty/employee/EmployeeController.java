package jetty.employee;

import jetty.openapi.api.EmployeeApi;
import jetty.openapi.models.EmployeeAddressDTO;
import jetty.openapi.models.EmployeeDTO;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("${api.path}")
@AllArgsConstructor
@RestController
public class EmployeeController implements EmployeeApi {

    private final EmployeeService employeeService;
    private final EmployeeAddressService employeeAddressService;
    private final ModelMapper modelMapper;

    @Override
    public ResponseEntity<EmployeeDTO> employeeEmployeeIdGet(@PathVariable("employeeId") Integer employeeId) {
        Employee employee = employeeService.findEmployeeById(employeeId);
        EmployeeDTO employeeDTO = modelMapper.map(employee, EmployeeDTO.class);
        return new ResponseEntity<>(employeeDTO, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<EmployeeAddressDTO> employeeAddressEmployeeIdGet(@PathVariable("employeeId") Integer employeeId) {
        EmployeeAddress employeeAddress = employeeAddressService.findEmployeeAddressById(employeeId);
        EmployeeAddressDTO employeeAddressDTO = modelMapper.map(employeeAddress, EmployeeAddressDTO.class);
        return new ResponseEntity<>(employeeAddressDTO, HttpStatus.OK);
    }

}
