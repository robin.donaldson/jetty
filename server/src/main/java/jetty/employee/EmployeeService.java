package jetty.employee;

import jetty.exception.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeService {

    public Employee findEmployeeById(Integer id) {

        List<Employee> employees = new ArrayList<>();

        employees.add(new Employee(1, "Robin", "Donaldson", 32));
        employees.add(new Employee(2, "Jane", "Smith", 25));
        employees.add(new Employee(3, "Alex", "Johnson", 28));
        employees.add(new Employee(4,"Emily", "Davis", 32));
        employees.add(new Employee(5,"Michael", "Wilson", 35));
        employees.add(new Employee(6,"Sara", "Taylor", 27));
        employees.add(new Employee(7,"David", "Brown", 31));
        employees.add(new Employee(8,"Olivia", "Thomas", 29));
        employees.add(new Employee(9,"Daniel", "Clark", 33));
        employees.add(new Employee(10,"Sophia", "Walker", 26));

        return employees.stream()
                .filter(employee -> employee.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new NotFoundException("Employee with ID: " + id + " not found!"));

    }

}
