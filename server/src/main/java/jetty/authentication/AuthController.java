package jetty.authentication;

import jetty.openapi.api.AuthenticateApi;
import jetty.openapi.models.CredentialsDTO;
import jetty.openapi.models.JwtDTO;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("${api.path}")
@AllArgsConstructor
@RestController
public class AuthController implements AuthenticateApi {

    private final TokenService tokenService;
    private final AuthenticationManager authenticationManager;

    @Override
    public ResponseEntity<JwtDTO> authenticatePost(@RequestBody CredentialsDTO credentialsDTO) throws AuthenticationException {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(credentialsDTO.getUsername(), credentialsDTO.getPassword()));
        return new ResponseEntity<>(tokenService.generateToken(authentication), HttpStatus.OK);
    }

}
