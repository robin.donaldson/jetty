# Build Instructions

- Install Docker 
- Open terminal
- Navigate to jetty directory
- Execute: docker compose up
- Wait for App to build
- Navigate to localhost
- Sign in with:
  - Username: robindonaldson
  - Password: password