// proxy - when in dev mode, requests containing '/api' will have http://localhost:4200 substituted for http://localhost:8080 to resolve CORS issue.
const PROXY_CONFIG = [
  {
    context: [
      "/api",
      "/logout"
    ],
    target: "http://localhost:8080",
    secure: false,
    loglevel: "debug"
  }
]

module.exports = PROXY_CONFIG;
