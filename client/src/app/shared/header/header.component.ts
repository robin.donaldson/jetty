import {Component, OnInit} from '@angular/core';
import {LogoutService} from "../../authentication/logout.service";
import {NavigationEnd, Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isNotLoginPage = false;

  constructor(public logoutService: LogoutService,
              private router: Router) {}

  ngOnInit() {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.isNotLoginPage = this.router.url !== '/login';
      }
    });
  }

}
