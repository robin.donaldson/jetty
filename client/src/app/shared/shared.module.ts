import {NgModule} from "@angular/core";
import {ErrorComponent} from './error/error.component';
import {HeaderComponent} from './header/header.component';
import {NgIf} from "@angular/common";

@NgModule({
  imports: [
    NgIf
  ],
  providers: [],
  declarations: [
    ErrorComponent,
    HeaderComponent
  ],
  exports: [
    ErrorComponent,
    HeaderComponent,
  ],
  entryComponents: [],
})

export class SharedModule {}
