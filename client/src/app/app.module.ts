import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { LoginComponent } from './routes/login/login.component';
import {AppRoutingModule} from "./routes/app-routing.module";
import {ApiModule, Configuration} from "./openapi";
import {environment} from "../environments/environment";
import { MyAccountComponent } from './routes/home/my-account/my-account.component';
import {SharedModule} from "./shared/shared.module";
import {AuthInterceptor} from "./authentication/auth.interceptor";
import { HomeComponent } from './routes/home/home.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

function ApiConfig(): Configuration {
  return new Configuration({
    basePath: environment.basePath
  });
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MyAccountComponent,
    HomeComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ApiModule.forRoot(ApiConfig),
    ReactiveFormsModule,
    SharedModule,
    FormsModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
