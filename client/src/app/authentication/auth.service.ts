import {Injectable} from '@angular/core';
import {Observable, tap} from "rxjs";
import {Credentials, DefaultService, Jwt} from "../openapi";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private apiGateway: DefaultService) { }

  // The requirements said token generation should be a GET request,
  // I've changed this to a POST request for security reasons.
  generateToken(credentials: Credentials): Observable<Jwt> {
    return this.apiGateway.authenticatePost(credentials).pipe(
      tap((res) => {
        // Insecure storage of JWT, XSS vulnerability - DO NOT USE IN PRODUCTION!
        sessionStorage.setItem('jwt', res.token);
      })
    );
  }

}
