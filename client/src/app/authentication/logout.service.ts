import { Injectable } from '@angular/core';
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class LogoutService {

  constructor(private router: Router) { }

  logout() {

    // Clear JWT from session storage
    sessionStorage.removeItem('jwt');

    // Redirect user to login page
    this.router.navigate(['/login']);

  }

}
