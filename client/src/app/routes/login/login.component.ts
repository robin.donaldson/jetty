import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from "../../authentication/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm!: FormGroup;

  constructor(private router: Router,
              private authService: AuthService) {}

  ngOnInit() {
    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    });
  }

  login() {

    if (this.loginForm.valid) {

      this.authService.generateToken(this.loginForm.value).subscribe({
        next: (res) => { // On success
          this.router.navigate(['/']); // Redirect to home page
        },
        error: (err) => { // On error
          alert('Invalid credentials'); // Display error
        }
      });

    }
  }
}

