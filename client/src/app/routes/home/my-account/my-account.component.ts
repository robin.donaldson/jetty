import {Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';
import {combineLatest, of, Subject, takeUntil} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {DefaultService, Employee, EmployeeAddress} from "../../../openapi";
import {animate, style, transition, trigger} from "@angular/animations";

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.scss'],
  animations: [
    trigger('fadeIn', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('500ms', style({ opacity: 1 })),
      ]),
    ]),
  ],
})
export class MyAccountComponent implements OnChanges, OnDestroy {

  @Input()
  public employeeId: number = 1;

  public employeeErrorMsg: string | null = null;
  public employeeAddressErrorMsg: string | null = null;
  public combinedInfo: CombinedInfo | null = null;

  private destroy$ = new Subject<void>();

  constructor(private apiGateway: DefaultService) { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['employeeId']) {
      this.loadEmployeeInformation();
    }
  }

  loadEmployeeInformation() {

    // clear previous employee and error messages
    this.combinedInfo = null;
    this.employeeErrorMsg = null;
    this.employeeAddressErrorMsg = null;

    // Load employee info
    const employee$ = this.apiGateway.employeeEmployeeIdGet(this.employeeId)
      .pipe(catchError((error) => {
        this.employeeErrorMsg = 'Error loading employee details.';
        return of({}); // return an empty object
      }));

    // Load employee address
    const address$ = this.apiGateway.employeeAddressEmployeeIdGet(this.employeeId)
      .pipe(catchError((error) => {
        this.employeeAddressErrorMsg = 'Error loading employee address.';
        return of({}); // return an empty object
      }));

    // Combine employee info, display partial info if an api call failed
    combineLatest([employee$, address$])
      .pipe(takeUntil(this.destroy$))
      .subscribe(([employee, address]) => {
        this.combinedInfo = {...employee, ...address};
      });

  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}

interface CombinedInfo extends Employee, EmployeeAddress {}
